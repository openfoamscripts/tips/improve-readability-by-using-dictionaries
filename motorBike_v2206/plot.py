# Copyright (c) 2023 Masahiro TANAKA
# Released under the MIT license
# https://gitlab.com/openfoamscripts/tips/improve-readability-by-using-dictionaries/-/blob/main/LICENSE

from io import StringIO
import os
import re
import matplotlib.pyplot as plt
import pandas as pd


class Plot2DGraph():
    pwd = os.path.dirname(__file__)

    def get_residuals(self):
        file = open(f'{self.pwd}/postProcessing/solverInfo/0/solverInfo.dat', 'r')
        planetxt = file.read()
        file.close()

        planetxt = re.sub('^# ', '', planetxt, flags=re.MULTILINE)

        df = pd.read_csv(StringIO(planetxt), delim_whitespace=True, skiprows=1)
        series = df.loc[:, ['p_initial', 'Ux_initial', 'Uy_initial', 'Uz_initial', 'k_initial', 'omega_initial']]
        return series


    def get_drag_coefficient(self):
        file = open(f'{self.pwd}/postProcessing/forceCoeffs1/0/coefficient.dat', 'r')
        planetxt = file.read()
        file.close()

        planetxt = re.sub('^# ', '', planetxt, flags=re.MULTILINE)

        df = pd.read_csv(StringIO(planetxt), delim_whitespace=True, skiprows=12)
        series = df.loc[:, ['Cd']]
        return series


    def plot_residuals(self, residuals):
        residuals.plot()
        plt.xlabel('iteration')
        plt.ylabel('residuals [-]')
        plt.semilogy()
        plt.xlim(0, 500)
        plt.ylim(1e-7, 1)
        plt.grid()
        plt.savefig(f'{self.pwd}/postProcessing/png/residuals.png')
        plt.close()
        return


    def plot_drag_coefficient(self, cd):
        cd.plot()
        plt.xlabel('iteration')
        plt.ylabel('drag coefficient [-]')
        plt.xlim(0, 500)
        plt.ylim(0, 1)
        plt.grid()
        plt.savefig(f'{self.pwd}/postProcessing/png/drag_coefficient.png')
        plt.close()
        return


if __name__ == '__main__':
    p = Plot2DGraph()
    residuals = p.get_residuals()
    cd = p.get_drag_coefficient()
    # print(residuals)
    # print(cd)

    p.plot_residuals(residuals)
    p.plot_drag_coefficient(cd)
