#!/bin/sh
cd "${0%/*}" || exit                                # Run from this directory
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions        # Tutorial run functions
#------------------------------------------------------------------------------

# Alternative decomposeParDict name:
decompDict="-decomposeParDict system/dicts/parallelization/decomposeParDict"
meshDict="-dict system/dicts/meshing"
## Standard decomposeParDict name:
# unset decompDict

# copy motorbike surface from resources directory
mkdir -p constant/triSurface

cp -f \
    "$FOAM_TUTORIALS"/resources/geometry/motorBike.obj.gz \
    constant/triSurface/

runApplication surfaceFeatureExtract ${meshDict}/surfaceFeatureExtractDict

runApplication blockMesh ${meshDict}/blockMeshDict

runApplication $decompDict decomposePar

# Using distributedTriSurfaceMesh?
if foamDictionary -entry geometry -value system/dicts/meshing/snappyHexMeshDict | \
   grep -q distributedTriSurfaceMesh
then
    echo "surfaceRedistributePar does not need to be run anymore"
    echo " - distributedTriSurfaceMesh will do on-the-fly redistribution"
fi

runParallel $decompDict snappyHexMesh -overwrite ${meshDict}/snappyHexMeshDict

runParallel $decompDict topoSet ${meshDict}/topoSetDict

#- For non-parallel running: - set the initial fields
# restore0Dir

#- For parallel running: set the initial fields
restore0Dir -processor

runParallel $decompDict patchSummary

runParallel $decompDict potentialFoam -writephi

runParallel $decompDict checkMesh -writeFields '(nonOrthoAngle)' -constant

runParallel $decompDict $(getApplication)

runApplication reconstructParMesh -constant

runApplication reconstructPar -latestTime

mkdir postProcessing/png/
python plot.py

#------------------------------------------------------------------------------
