# OpenFOAMのdictionaryを用いた可読性の向上

作成: M. TANAKA
日付: 2022.11.17

:warning: このREADMEの文章は未完成です。内容を理解するためにはslide.odpを開いて見ることをお勧めします。

[TOC]

## 1. はじめに

:warning: このテキストで紹介するのは「私はこのやり方が便利に思った」というものです。今後も変化していくでしょうし、異論もあると思います。あくまで一例として参考になれば幸いです。

「こうした方がよい」「これは間違っている」といった意見は大歓迎です。
（できるか未確認ですが）issueなど立てて知らせていただければ。

### 1.1 OpenFOAMの計算設定の分かりにくさ

OpenFOAMで新規にシミュレーションを行うには、所定のフォーマット・構造の設定ファイル群を作成し、逐次的なコマンド操作によって種々の操作を実行します。注意すべき特徴として、

- 計算実行時にはTime, fvMeshなどの各種インスタンスが生成されるが、プロパティを設定する段階では単なるテキストファイルしか存在しない。したがって、プロパティを設定する際にはインスタンスが持つメンバー変数・関数や関連するインスタンスを参照できない。
- これは同じCUIでも、コンソールで対話的に操作するタイプのもの（例: FreeCADCmd, pvpython）とは根本的に異なる。

があります。この方式は設定値を変更する際にコンパイルし直さずに済むようにするために採用されているらしいです[^1]。（疑問: FreeCADもコアはC++で書かれていますが、APIはPythonで用意されているのでコンパイルせずに対話的に操作できます。OpenFOAMでもそうしなかったのはCADに比べ計算量が格段に多いことが何らかの障壁になったのでしょうか。）

ここから私が感じるOpenFOAMの使いにくさとして、

- プロパティの設定に複数の誤りが含まれていたとしても、ユーザがそれに気付けるのは計算終了後にログファイルを見たときである。
- したがって、ユーザが設定ファイルのプロパティと計算手続きとの対応関係を把握するまでに時間がかかる。

が挙げられます。

### 1.2 分かりにくさをどう軽減するか

上記のような使いにくさに対処するにはいくつか方法が考えられ、ここで述べるより適切で強力な方法も多々あるでしょうが、ここでは対症療法的に「情報の一覧性を高める＆重複をなくす」方向で考えます。

<!-- - 実際のインスタンスはどんな構造になっている？
- 情報の一覧性を高める　＆　重複をなくす -->

### 1.3 目的

- 情報の一覧性、単一性を高めることで、OpenFOAMケースディレクトリを読みやすく、間違えにくくする。
- (ついでに)商用ソフトと同等の正確な計算結果を安定して得るTipsを紹介する。
- 対象とするチュートリアルケースは[moterBike](https://develop.openfoam.com/Development/openfoam/-/tree/OpenFOAM-v2206/tutorials/incompressible/simpleFoam/motorBike)。

### 1.4 注意した点

OpenFOAMには2つのバージョンが存在します：

- ESI version
    - [Home Page](https://www.openfoam.com/)
    - [User Guide](https://www.openfoam.com/documentation/user-guide)
    - [Code Repository](https://develop.openfoam.com/Development/openfoam)
- foundation version
    - [Home Page](https://www.openfoam.org/)
    - [User Guide](https://doc.cfd.direct/openfoam/user-guide-v10/contents)
    - [Code Repository](https://github.com/OpenFOAM/OpenFOAM-dev)

ここではESI版のみを取り上げます。適宜修正すれば、Foundation版でも同じように動くと思います。

## 2. 手法: 

### 2.1 OpenFOAMの"dictionary"とは


## 3. 実践: dictionaryを用いたケースフォルダの書き換え

### 3.1 Gather fluid properties in a single file

### 3.2 Gather boundary conditions in a single file


## 4. その他のTips

### 4.1 systemフォルダを整理する

#### 4.1.1 functionsをサブフォルダ化

`$FOAM_CASE/system`直下のファイルが多すぎて一覧性が悪いので、一部を新規サブフォルダに格納する。`postProcess`に関わる`functions`ファイルを

```
<system>/myFunctions # short
$FOAM_CASE/system/myFunctions # formal (valid on v10 too)

#include "myFunctions/streamLines"
```

:warning: 注意

- このように階層を変えると、`simpleFoam postProcess -func streamLines -LatestTime`のように解析後にfunction objectを実行することができなくなる。run-timeでは動くのでほぼ問題ないが、`0`フォルダに出力されたpotentialFoamの結果を処理したい場合などに問題となる。

#### 4.1.2 "-dict"オプションを利用

"(コマンド名)Dict"という設定ファイルを必要とする（おそらく全ての）コマンドには、Dictファイルを指定の場所から実行する"-dict"オプションが用意されている。

### 4.2 vary difference scheme within the simulation

function objectの[timeActivatedFileUpdate](https://www.openfoam.com/documentation/guides/latest/api/classFoam_1_1functionObjects_1_1timeActivatedFileUpdate.html#details)という機能を使うと、

#### 参考

- [How to update OpenFOAM numerical settings automatically at runtime](https://solution.esi.co.jp/openfoam/blog/how-to-update-openfoam-numerical-settings-automatically-at-runtime)
- [windshieldCondensation](https://develop.openfoam.com/Development/openfoam/-/blob/OpenFOAM-v2206/tutorials/heatTransfer/chtMultiRegionFoam/windshieldCondensation/system/controlDict)

[^1]: 株式会社テラバイト『OpenFOAMライブラリリファレンス』，2020，p209．
